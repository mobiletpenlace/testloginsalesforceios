//
//  ViewController.swift
//  SalesForceTest
//
//  Created by Charls Salazar on 31/10/19.
//  Copyright © 2019 PureDump. All rights reserved.
//

import UIKit
import ObjectMapper
import AlamofireObjectMapper
import Alamofire
import PKHUD

class ViewController: UIViewController {
    var request : Alamofire.Request?
    @IBOutlet weak var mNameTextField: UITextField!
    var mURLSF : String = "https://cs3.salesforce.com/services/apexrest/WS_ApiTestApp"
    var mURLLoginSF : String = "https://cs3.salesforce.com/services/oauth2/token"
    public static let CUSTOM_HEADERS = ["Content-Type": "application/x-www-form-urlencoded"]
    static var TOKEN : String = ""
    public static let CUSTOM_HEADERS_SALESFORCE = [
        "Accept": "application/json; charset=UTF-8",
        "Content-Type": "application/json; charset=UTF-8",
        "Authorization": "Bearer " + TOKEN
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func mActionConsumingButton(_ sender: Any) {
        loginSF()
    }
    
    //MARK:- Login with salesforce
    
    func loginSF(){
        AlertDialog.showOverlay()
        let registerRequest : LoginSFRequest = LoginSFRequest(mUserName:"acerone@totalplay.com.mx.qa" , mPassword: "totalplay123456")
        let params  = Mapper().toJSON(registerRequest)
        
        request = Alamofire.request(mURLLoginSF, method: HTTPMethod.post, parameters: params, encoding: URLEncoding.queryString, headers: ViewController.CUSTOM_HEADERS).responseObject {
            (response: DataResponse<LoginSFResponse>) in
            
            switch response.result {
            case .success:
                let objectReponse : LoginSFResponse = response.result.value!
                print("Access Token", objectReponse.mAccesToken)
                if(objectReponse.mAccesToken != ""){
                    ViewController.TOKEN = objectReponse.mAccesToken
                    self.consumingWS(mName: self.mNameTextField.text!)
                }else{
                    AlertDialog.hideOverlay()
                    self.showMessage(mMessage: "No se pudo obtener el token reitentar")
                }
            case .failure(_):
                AlertDialog.hideOverlay()
                self.showMessage(mMessage: "Error de Comunicacion con SF")
                break
            }
        }
    }
    //MARK:- Consuming API
    
    func consumingWS(mName : String){
        let registerRequest : HumanPeopleRequest!
        if(mName != ""){
            registerRequest = HumanPeopleRequest(mNombre: mName)
        }else{
            registerRequest = HumanPeopleRequest(mNombre: "Carlos")
        }
        
        let params  = Mapper().toJSON(registerRequest)
        
        request = Alamofire.request(mURLSF, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ViewController.CUSTOM_HEADERS_SALESFORCE).responseObject {
            (response: DataResponse<SalesForceResponse>) in
            print(response.result)
            switch response.result {
            case .success:
                AlertDialog.hideOverlay()
                let objectReponse : SalesForceResponse = response.result.value!
                self.showMessage(mMessage: objectReponse.mMensaje)
            case .failure(_):
                AlertDialog.hideOverlay()
                self.showMessage(mMessage: "Error de Comunicacion con SF")
                break
            }
        }
    }
    
    func showMessage(mMessage : String){
        let alert = UIAlertController(title: "Respuesta", message: mMessage, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK: - Pojos Request

class HumanPeopleRequest: NSObject, Mappable{
    var mNombre : String = ""
    
    init(mNombre : String){
        self.mNombre = mNombre
    }
    
    public required init?(map: Map) {
           
    }
       
    internal func mapping(map : Map){
        mNombre <- map["nombre"]
    }
}

class LoginSFRequest : NSObject, Mappable{
    var mGrantType : String = ""
    var mClient_id : String = ""
    var mClient_secret : String = ""
    var mUserName : String = ""
    var mPassword : String = ""
    
    init(mUserName : String, mPassword: String){
        self.mGrantType = "password"
        self.mClient_id = "3MVG9pHRjzOBdkd_3vq.kUaru1tWOEBM1FSkjuQHxFV1E6uzp4APCRFJRpiDFw8A7CbFm3tmJeYtXYMvPX7Pf"
        self.mClient_secret = "3259493245466619197"
        self.mUserName = mUserName
        self.mPassword = mPassword
    }
    
    required init?(map: Map) {
              
    }
    
    func mapping(map: Map) {
        self.mGrantType <- map["grant_type"]
        self.mClient_id <- map["client_id"]
        self.mClient_secret <- map["client_secret"]
        self.mUserName <- map["username"]
        self.mPassword <- map["password"]
    }
}

//MARK: - Pojos Response
class SalesForceResponse: NSObject, Mappable{
   
    
    var mResult : String = ""
    var mResultDescription : String = ""
    var mMensaje : String = ""
       
    required init?(map: Map) {
           
    }
    
    internal func mapping(map : Map){
         mResult <- map["result"]
         mResultDescription <- map["resultDescription"]
         mMensaje <- map["mensaje"]
    }
}


class LoginSFResponse : NSObject , Mappable {
    var mAccesToken : String = ""
    var mInstanceUrl : String = ""
    var mId : String = ""
    var mTokenType : String = ""
    var mIssued_at : String = ""
    var mSignature : String = ""
    
    required init?(map: Map) {
           
    }
    
    func mapping(map: Map) {
        self.mAccesToken <- map["access_token"]
        self.mInstanceUrl <- map["instance_url"]
        self.mId <- map["id"]
        self.mTokenType <- map["token_type"]
        self.mIssued_at <- map["issued_at"]
        self.mSignature <- map["signature"]
    }
}

//MARK: AlertDialog

class AlertDialog {
    
    static var overlay : UIView?
    static var viewController : UIViewController?
    
    static func show
        
        (title: String, body: String, view : UIViewController ){
        let refreshAlert = UIAlertController(title: title, message: body, preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
        }))
        view.present(refreshAlert, animated: true, completion: nil)
    }
    
    @objc static func pressed(){
        
    }
    
    static func showOverlay(){
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
    }
    
    static func hideOverlay(){
        PKHUD.sharedHUD.hide(true)
    }
    
}

